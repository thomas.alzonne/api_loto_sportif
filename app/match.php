<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class match extends Model
{
    protected $fillable = ['team1','team2','result','date'];

}
