<?php

namespace App\Http\Controllers;

use App\match;
use Illuminate\Http\Request;
use App\team;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $match= new match();
        $match->team1 = team::where('name','=', $request->input('team1'))->value('id');
        $match->team2 = team::where('name','=', $request->input('team2'))->value('id');
        $match->date = $request->input('date');
        $match->result = $request->input('result');
        $match->save();
        return "Match created";

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\match  $match
     * @return \Illuminate\Http\Response
     */
    public function show(match $match)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\match  $match
     * @return \Illuminate\Http\Response
     */
    public function edit(match $match)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\match  $match
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $newteam1 = team::where('name','=', $request->input('newteam1'))->value('id');
        $newteam2 = team::where('name','=', $request->input('newteam2'))->value('id');
        $newdate = $request->input('newdate');
        $newresult = $request->input('newresult');
        $match = match::where('id','=', $id)->get()->first()->update(array(
                         'team1'=>$newteam1,
                         'team2'=>$newteam2,
                         'matchdate'=>$newdate,
                         'result'=>$newresult,
                       ));
        return "Match updated" ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\match  $match
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        $match = match::where('id','=', $id)->get()->first()->delete();
        return "Match deleted";
    }

    public function getAll(){
      $matches = match::all();
      $array =[];
      foreach ($matches as $match) {
        $id1 = $match->team1;
        $id2 = $match->team2;
        $name1 = team::where('id','=',$id1)->value('name');
        $name2 = team::where('id','=',$id2)->value('name');
        array_push($array,["team 1 : ".$name1, "team 2 : ".$name2,$match]);
      }
      return $array;
    }

    public function getIncMatchs(){
      $matches = match::all()->where('result','=', null);
      $array =[];
      foreach ($matches as $match) {
        $id1 = $match->team1;
        $id2 = $match->team2;
        $name1 = team::where('id','=',$id1)->value('name');
        $name2 = team::where('id','=',$id2)->value('name');
         array_push($array,["team 1 : ".$name1, "team 2 : ".$name2,$match]);
      }
      return $array;
    }

    public function getDoneMatchs(){
      $matches = match::where('result','!=', null)->get();
      $array = [];
      foreach ($matches as $match) {
        $id1 = $match->team1;
        $id2 = $match->team2;
        $name1 = team::where('id','=',$id1)->value('name');
        $name2 = team::where('id','=',$id2)->value('name');
        array_push($array, ["team 1 : ".$name1, "team 2 : ".$name2,$match]);
      }
      return $array;
    }

    public function updateScore(Request $request){
      $id = $request->input('id');
      $result = $request->input('result');
      $match = match::where('id','=', $id)->get()->first()->update(array(
          'result'=>$result,
          ));
          return "Result updated";
    }
}
