<?php

namespace App\Http\Controllers;

use App\team;
use Illuminate\Http\Request;
use App\match;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $team = new team();
      $team->name = $request->input('name');
      $team->save();
      return "Team created";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, team $team)
    {
        $newname = $request->input('newname');
        $team = team::where('name','=', $request->input('name'))->get()->first()->update(array(
                         'name'=>$newname,
                       ));
        return "Team updated";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $team = team::where('name','=',$request->input('name'))->get()->first()->delete();
        return "Team deleted";
    }

    public function getRank(){
      $classement = [];
      $teams = team::all();
      foreach ($teams as $team) {
        $points = 0;
        $team_matchshomes = match::where('team1','=',$team->id)->get();
        foreach ($team_matchshomes as $team_matchshome) {
          if($team_matchshome->result == 1){
            $points += 2;
          }
          if ($team_matchshome->result == 'N'){
            $points += 1;
          }
        }
        $team_matchsaways = match::where('team2','=',$team->id)->get();
        foreach ($team_matchsaways as $team_matchsaway) {
          if($team_matchsaway->result == 2){
            $points += 2;
          }
          if ($team_matchsaway->result == 'N'){
            $points += 1;
          }
        }
      array_push($classement, [$team->name,$points]);
      }
      for ($i=0; $i < count($classement)-1 ; $i++) {
        if ($classement[$i][1]< $classement[$i+1][1]) {
            $arraychanger=[];
            array_push($arraychanger,$classement[$i]);
            $classement[$i]=$classement[$i+1];
            $classement[$i+1] = $arraychanger[0];
            $i=0;
        }
      }
      return $classement;
    }
}
