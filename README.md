# API Loto Sportif

Bonjour, voila mon prjet API Loto Sportif, j'ai tout fait sauf la partie auth.
Il y a:

    -liste des matchs à venir (/matchs/incoming), des matchs déjà joués et des résultats (/matchs/done) (3 points)
    -Classement des équipes (/teams/rank) (Points: Perdu:0, Nul:1, Gagné:2) (2 points) 
    -création / modification (par nom) / suppression d'une équipe (par nom)  (/teams) (3 points) 
    -création / modification (par id) / suppression d'un match (par id)  (/matchs) (3 points)
    -liste des matchs (/matchs)  (2 points)
    -Ajout de résultats (/matchs/updatescore) (2 points)