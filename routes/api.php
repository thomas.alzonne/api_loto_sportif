<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// CRUD TEAM
Route::post('teams/create', 'TeamController@create');  //done
Route::post('teams/update', 'TeamController@update');  //done
Route::post('teams/delete', 'TeamController@destroy'); //done

//CRUD MATCHS
Route::post('matchs/create', 'MatchController@create'); //done
Route::post('matchs/update', 'MatchController@update'); //done
Route::post('matchs/delete', 'MatchController@destroy'); //done
Route::post('matchs/updatescore', 'MatchController@updateScore'); //done

//AFFICHAGE
Route::get('matchs/incoming','MatchController@getIncMatchs'); //done
Route::get('matchs/done','MatchController@getDoneMatchs'); //done
Route::get('matchs', 'MatchController@getAll'); //done
Route::get('teams/rank', 'TeamController@getRank'); //done
